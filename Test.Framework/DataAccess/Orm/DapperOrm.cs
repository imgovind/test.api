﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Test.Framework.Extensions;
using Test.Framework.Extensibility;

namespace Test.Framework.DataAccess
{
    public class DapperOrm : Disposable, IOrm
    {
        #region Private Members

        private string connectionName;

        #endregion

        #region Constructor

        public DapperOrm(string connectionName)
        {
            this.connectionName = connectionName;
        }

        public IDbConnection GetConnection()
        {
            return new Connection(this.connectionName).Get();
        }

        #endregion

        #region IDataAccessConnection Members

        public bool Execute(SqlCommand query)
        {
            using (IDbConnection dbConnection = new Connection(this.connectionName).Get())
            {
                var result = dbConnection.Execute(dbConnection.CreateDapperCommand(query)) > 0 ? true : false;
                return result;
            }
        }

        public async Task<bool> ExecuteAsync(SqlCommand query)
        {
            using (IDbConnection dbConnection = new Connection(this.connectionName).Get())
            {
                var result = await dbConnection.ExecuteAsync(dbConnection.CreateDapperCommand(query)) > 0;
                return result;
            }
        }

        public bool Execute(IList<SqlCommand> queries)
        {
            using (IDbConnection dbConnection = new Connection(this.connectionName).Get())
            {
                if (queries == null || !queries.Any()) return false;
                var errorMessage = string.Empty;
                using (var transaction = dbConnection.BeginTransaction())
                {
                    try
                    {
                        if (queries.Count == 1 && queries.First() != null)
                            dbConnection.Execute(dbConnection.CreateDapperCommand(queries.First()));
                        queries.ForEach(query =>
                        {
                            errorMessage = query.ErrorMessage;
                            dbConnection.Execute(dbConnection.CreateDapperCommand(query));
                        });
                        transaction.Commit();
                        return true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        //Log this exception
                        return false;
                    }
                }
            }
        }

        public async Task<bool> ExecuteAsync(IList<SqlCommand> queries)
        {
            using (IDbConnection dbConnection = new Connection(this.connectionName).Get())
            {
                var result = false;

                if (queries == null || !queries.Any())
                    return result;

                var errorMessage = string.Empty;
                List<Task<int>> transactionalQueries = new List<Task<int>>();
                using (var transaction = dbConnection.BeginTransaction())
                {
                    try
                    {
                        if (queries.Count == 1 && queries.First() != null)
                        {
                            result = await dbConnection.ExecuteAsync(dbConnection.CreateDapperCommandAsync(queries.First())) > 0;
                        }
                        else
                        {
                            queries.ForEach(query =>
                            {
                                errorMessage = query.ErrorMessage;
                                transactionalQueries.Add(dbConnection.ExecuteAsync(dbConnection.CreateDapperCommandAsync(query)));
                            });

                            var results = await Task.WhenAll(transactionalQueries);
                            result = true;
                        }
                        transaction.Commit();
                        return result;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        //Log this exception
                        return result;
                    }
                }
            }
        }

        public IEnumerable<T> Select<T>(SqlCommand query, ISelectable<T> traits = null)
            where T : class, new()
        {
            using (IDbConnection dbConnection = new Connection(this.connectionName).Get())
            {
                if (traits == null) return dbConnection.Query<T>(dbConnection.CreateDapperCommand(query));
                return SelectWithTraits<T>(query, traits);
            }
        }

        public async Task<IEnumerable<T>> SelectAsync<T>(SqlCommand query, ISelectable<T> traits = null)
            where T : class, new()
        {
            using (IDbConnection dbConnection = new Connection(this.connectionName).Get())
            {
                IEnumerable<T> result = null;

                if (traits == null)
                    result = await dbConnection.QueryAsync<T>(dbConnection.CreateDapperCommandAsync(query));
                else
                    result = await SelectWithTraitsAsync<T>(query, traits);

                return result;
            }
        }

        public IEnumerable<T> Select<T>(SqlCommand query, Func<DataReader, T> readMapper)
            where T : class, new()
        {
            using (IDbConnection dbConnection = new Connection(this.connectionName).Get())
            {
                using (IDataReader dataReader = dbConnection.CreateCustomCommand(query).ExecuteReader(CommandBehavior.CloseConnection))
                {
                    var result = dataReader.Hydrate<T>(readMapper);
                    return result;
                }
            }
        }

        public async Task<IEnumerable<T>> SelectAsync<T>(SqlCommand query, Func<DataReader, T> readMapper)
            where T : class, new()
        {
            using (IDbConnection dbConnection = new Connection(this.connectionName).Get())
            {
                var result = await Task.Run<IEnumerable<T>>(() => Select<T>(query, readMapper));
                return result;
            }
        }

        #endregion

        #region Private Methods

        private IEnumerable<T> SelectWithTraits<T>(SqlCommand query, ISelectable<T> traits)
            where T : class, new()
        {
            using (IDbConnection dbConnection = new Connection(this.connectionName).Get())
            {
                using (IDataReader dataReader = dbConnection.CreateCustomCommand(query).ExecuteReader(CommandBehavior.CloseConnection))
                {
                    var result = dataReader.Hydrate<T>(traits);
                    return result;
                }
            }
        }

        private async Task<IEnumerable<T>> SelectWithTraitsAsync<T>(SqlCommand query, ISelectable<T> traits)
            where T : class, new()
        {
            var result = await Task.Run<IEnumerable<T>>(() => SelectWithTraits<T>(query, traits));
            return result;
        }

        #endregion
    }
}

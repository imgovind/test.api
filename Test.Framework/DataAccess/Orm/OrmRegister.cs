﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Test.Framework.Configuration;
using Test.Framework.Extensibility;
using MySql.Data.MySqlClient;
using LightInject;

namespace Test.Framework.DataAccess
{
    public static class OrmRegister
    {
        public static void Register(IList<string> connectionNames, OrmType ormType)
        {
            switch (ormType)
            {
                case OrmType.Custom:
                    CustomRegister(connectionNames);
                    break;
                case OrmType.Dapper:
                    DapperRegisterLightInject(connectionNames);
                    break;
                default:
                    DapperRegisterLightInject(connectionNames);
                    break;
            }
        }

        private static void CustomRegister(IList<string> connectionNames)
        {
            var configuration = Container.Resolve<IWebConfiguration>();
            connectionNames.ToList().ForEach(connectionName =>
            {
                Container.RegisterInstance<IOrm, CustomOrm>(
                    connectionName, 
                    new CustomOrm(Container.Resolve<IDbConnection>(connectionName)), 
                    ObjectLifeSpans.Singleton);
            });
        }

        private static void DapperRegister(IList<string> connectionNames)
        {
            connectionNames.ToList().ForEach(connectionName =>
            {
                Container.RegisterInstance<IOrm, DapperOrm>(
                    connectionName,
                    new DapperOrm(connectionName),
                    ObjectLifeSpans.Singleton);
            });
        }

        private static void DapperRegisterLightInject(IList<string> connectionNames)
        {
            connectionNames.ToList().ForEach(connectionName =>
            {
                var container = (ServiceContainer)Container.resolver.GetUnderlyingContainer();
                container.Register<IOrm>(factory => 
                    new DapperOrm(connectionName), 
                    connectionName, 
                    new PerContainerLifetime());
            });
        }
    }
}

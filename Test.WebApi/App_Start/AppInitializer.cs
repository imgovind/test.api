﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.WebApi.Middleware;
using Test.Data;
using Test.Framework.Extensibility;
using Test.Identity;

namespace Test.WebApi
{
    public static class AppInitializer
    {
        public static void Initialize()
        {
            Test.Framework.Framework.Initialize();
            DataAccessLayer.Initialize();
            TestIdentityProvider.Initialize();
            Container.Register<ISigningCredentialsProvider, SigningCredentialsProvider>();
        }
    }
}
﻿using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using Test.WebApi.Extensions.Routes;
using Test.WebApi.ResponseEnrichers;
using Test.Framework.Handlers.Resource;

namespace Test.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.MessageHandlers.Add(new CompressionHandler());
            //config.MessageHandlers.Add(new DecompressionHandler());
            //config.MessageHandlers.Add(new GZipToJsonHandler());
            config.MessageHandlers.Add(new EnrichingHandler());

            config.AddResponseEnrichers(new UserResponseEnricher());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.RegisterDefaultRoutes();

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            CreateMediaTypes(jsonFormatter);
        }

        private static void CreateMediaTypes(JsonMediaTypeFormatter jsonFormatter)
        {
            var mediaTypes = new string[] {
                "application/vnd.testangular.v1+json",
                "application/vnd.testangular.v2+json",
                "application/vnd.testangular.v3+json"
            };

            foreach (var mediaType in mediaTypes)
            {
                jsonFormatter.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue(mediaType));
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Test.WebApi.Extensions.Routes
{
    public static class RouteExtensions
    {
        public static void RegisterDefaultRoutes(this HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                 name: "DefaultApi",
                 routeTemplate: "api/{controller}/{id}",
                 defaults: new { id = RouteParameter.Optional }
            );
        }

        public static void RegisterAuthenticationRoutes(this HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                 name: "AuthenticationUser",
                 routeTemplate: "api/identity/user/{id}",
                 defaults: new { controller = "AuthenticationUser", id = RouteParameter.Optional }
            );
        }
    }
}
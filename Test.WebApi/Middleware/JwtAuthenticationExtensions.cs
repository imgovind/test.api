﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.WebApi.Middleware
{
    public static class JwtAuthenticationExtensions
    {
        public static IAppBuilder UseJwtAuthentication(this IAppBuilder app, JwtAuthenticationOptions options)
        {
            return app.Use<JwtAuthenticationMiddleware>(options);
        }
    }
}
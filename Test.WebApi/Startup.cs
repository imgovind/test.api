﻿using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Test.WebApi.Middleware;
using Test.Data;
using Test.Framework.Extensibility;
using Test.Identity;
using LightInject.WebApi;
using LightInject;
using System.Web.Http.Dispatcher;
using StackExchange.Redis;
using Test.Framework.Handlers.Caching.Server;
using Test.Framework.Handlers.Caching.EntityTagStores.Redis;

[assembly: OwinStartup(typeof(Test.WebApi.Startup))]
namespace Test.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AppInitializer.Initialize();
            ConfigureAuth(app);

            var config = new HttpConfiguration();

            var container = (ServiceContainer)Container.resolver.GetUnderlyingContainer();
            container.RegisterApiControllers();
            container.EnableWebApi(config);

            config.Services.Replace(typeof(IHttpControllerSelector), new CustomHttpControllerSelector(config));
            ConfigureCache(ref config);
            WebApiConfig.Register(config);
            app.UseCors(CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        private void ConfigureCache(ref HttpConfiguration config)
        {
            var options = new ConfigurationOptions();
            options.EndPoints.Add("localserver");
            options.Password = "23Eiyztygt";
            var connection = ConnectionMultiplexer.Connect(options);
            var redisEntityTagStore = new RedisEntityTagStore(connection);
            var redisCachingHandler = new CachingHandler(config, redisEntityTagStore, new string[] { "Accept" });
            config.MessageHandlers.Add(redisCachingHandler);
        }
    }
}
﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Framework.Extensibility;
using Test.Identity.Model;
using Test.Identity.Stores;
using Test.Framework;
using Test.Identity.Services;
using Test.Data;
using Test.Framework.DataAccess;
using Test.Framework.Configuration;

namespace Test.Scratch
{
    class Program
    {
        static void Main(string[] args)
        {
            Framework.Framework.Initialize();
            var configuration = Container.Resolve<IWebConfiguration>();
            List<string> connectionStringNames = configuration.GetConnectionStringNames().ToList();
            ConnectionRegister.Register(connectionStringNames, SqlDbmsType.MySql);
            OrmRegister.Register(connectionStringNames, OrmType.Dapper);
            DbRegister.Register(connectionStringNames, SqlDbmsType.MySql);
            Container.Register<IDataProvider, DataProvider>(ObjectLifeSpans.Singleton);
            //DataRegister.Initialize();
            //EntityMap.Initialize();
            Container.Register<IUserService<IdentityUser>, UserService<IdentityUser>>();
            Container.Register<IRoleService, RoleService>();
            Container.Register<IUserClaimsService, UserClaimsService>();
            Container.Register<IUserLoginsService, UserLoginsService>();
            Container.Register<IUserRolesService, UserRolesService>();
            Container.Register<IUserStore<IdentityUser>, UserStore<IdentityUser>>();
            try
            {
                var userStore = Container.Resolve<IUserStore<IdentityUser>>();
                var output = userStore.FindByNameAsync("devner89@gmail.com");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
